import React from "react";
import { Switch, Route } from "react-router-dom";
import Main from "../../components/Main/Main.js";
import Posts from "../../components/Posts/Posts.js";
import Login from "../../components/Login/Login.js";
import Register from "../../components/Register/Register.js";

export default class BelowNavBar extends React.Component {
  render() {
    return (
      <div style={{ marginTop: this.props.navBarHeight }}>
        <Switch>
          <Route exact path="/" component={Main} />

          <Route exact path="/posts" component={Posts} />

          <Route exact path="/login" component={Login} />

          <Route exact path="/register" component={Register} />
        </Switch>
      </div>
    );
  }
}
