import React from "react";

export default class Posts extends React.Component {
  // constructor() {
  //   super();
  // }

  componentDidMount() {
    const token = `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InJlZmFlbHNoIiwiaWF0IjoxNjM0ODQyMDgwfQ.U90hULurC2jjgeVIpr2R6VnBK1G-0yQaFzios5QsRixuREt8Dt03xCoU3Vxt7APHWONsblXXBsQBXoTIsv6SRQ`;
    const url = `https://salty-refuge-86855.herokuapp.com/api/v1/posts`;
    fetch(url, {
      method: `GET`,
      headers: { authorization: `Bearer ` + token },
    })
      .then((response) => response.json())
      .then((data) => console.log(data));
  }

  render() {
    return <div>Posts</div>;
  }
}
