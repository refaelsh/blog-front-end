import "./index.css";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { AppWithRouter } from "./App.js";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter basename="/blog-front-end">
      <AppWithRouter />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById(`root`)
);
