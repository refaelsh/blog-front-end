import React from "react";
import { withRouter } from "react-router";
import NavBar from "./components/NavBar/NavBar.js";
import BelowNavBar from "./components/BelowNavBar/BelowNavBar.js";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { bla: `main`, navBarHeight: 0 };
  }

  componentDidMount() {
    const height = document.getElementById(`navBarID`).clientHeight;
    const newState = this.state;
    newState.navBarHeight = height;
    this.setState(newState);
  }

  render() {
    return (
      <div>
        <NavBar />
        <BelowNavBar navBarHeight={-this.state.navBarHeight} />
      </div>
    );
  }
}

export const AppWithRouter = withRouter(App);
