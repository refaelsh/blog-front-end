import React from "react";
import { Link } from "react-router-dom";
import "./navbar.css";

export default class NavBar extends React.Component {
  render() {
    return (
      <div id="navBarID" className="nav-bar-div">
        <Link className="nav-bar-item" to="/posts">
          Posts
        </Link>
        <Link className="nav-bar-item" to="/login">
          Login
        </Link>
        <Link className="nav-bar-item" to="/register">
          Register
        </Link>
      </div>
    );
  }
}
