[![pipeline status](https://gitlab.com/refaelsh/blog-back-end/badges/master/pipeline.svg)](https://gitlab.com/refaelsh/blog-back-end/-/commits/master)
[![coverage report](https://gitlab.com/refaelsh/blog-back-end/badges/master/coverage.svg)](https://gitlab.com/refaelsh/blog-back-end/-/commits/master)
![](https://img.shields.io/gitlab/v/tag/refaelsh/blog-back-end)
[![](https://tokei.rs/b1/gitlab/refaelsh/blog-back-end?category=code)](https://gitlab.com/refaelsh/blog-back-end)
[![](https://tokei.rs/b1/gitlab/refaelsh/blog-back-end?category=blanks)](https://gitlab.com/refaelsh/blog-back-end)
[![](https://tokei.rs/b1/gitlab/refaelsh/blog-back-end?category=files)](https://gitlab.com/refaelsh/blog-back-end)
[![](https://tokei.rs/b1/gitlab/refaelsh/blog-back-end?category=lines)](https://gitlab.com/refaelsh/blog-back-end)
[![](https://tokei.rs/b1/gitlab/refaelsh/blog-back-end?category=comments)](https://gitlab.com/refaelsh/blog-back-end)

This is an assignment from the [Odin Project](https://www.theodinproject.com/paths/full-stack-javascript/courses/nodejs/lessons/blog-api): a Front End for REST API of a blogging site.
